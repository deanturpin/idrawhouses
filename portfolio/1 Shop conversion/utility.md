---
top: 66
left: 35
width: 33
---

# Utility room
Options were a little limited with this room. It was originally part of an
original extension with a low flat roof, consequently the ceiling height after
putting down the floor and insulation is only 2100mm.

