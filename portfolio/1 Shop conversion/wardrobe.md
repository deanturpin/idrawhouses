---
top: 60
left: 10
textcolor: fawn
---

# Wardrobe
Fitted wardrobe in the master bedroom, built by [Marcello Mirto](http://marcellomirto.com/carpentry/).

