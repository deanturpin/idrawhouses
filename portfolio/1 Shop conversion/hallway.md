---
top: 75
left: 50
width: 25
textcolor: #ffffff
---

# Hallway
The main hallway is 1.5m wide, but given the height of the ceiling any less would have felt unbalanced.
