---
top: 25
left: 66
width: 25
---

# Lighting

[Plumen Wilbur](https://ukshop.plumen.com/products/wilbur-ps60-2000k) bulbs used throughout. Fixtures from [Edwards and Hope (Brighton)](https://www.edwardsandhope.co.uk/lamp-holders).
